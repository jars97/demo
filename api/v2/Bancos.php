<?php

/**
 * Created by PhpStorm.
 * User: joserodriguez
 * Date: 17/02/17
 * Time: 08:09 PM
 */
class Bancos
{
    private static $tabla = 'maestrobancos';



    public function listAll($app){

        try {

            $obj = R::findAll(self::$tabla);

            return $obj;
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function create($app){
        try {
            $res=$app->response();
            $request = $app->request();
            $body = $request->getBody();
            $input = json_decode($body);

            $descripcion=(string) $input->descripcion;
            $esPos=(string) $input->esPos;
            $codigoAdm=(string) $input->codigoAdm;

            $obj = R::dispense(self::$tabla);
            $obj -> descripcion = $descripcion;
            $obj -> espos = $esPos;
            $obj -> codigoadm=$codigoAdm;
            $id=R::store($obj);

            $res->status(200);
            $arrRtn['mensaje'] = 'Operacion exitosa!!';
            echo json_encode($arrRtn);
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function readById($app,$id){
        try {

            $obj = R::findOne(self::$tabla,'id=?',array($id));
            if ($obj)
            {
                return $obj;
            }else{
                throw new Exception('No Existe el registro');
            }


        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function update($app,$id){
        try {
            $res=$app->response();

            $request = $app->request();
            $body = $request->getBody();
            $input = json_decode($body);
            $descripcion=(string) $input->descripcion;
            $esPos=(string) $input->esPos;
            $codigoAdm=(string) $input->codigoAdm;
            $obj = R::findOne(self::$tabla,'id=?',array($id));
            if ($obj)
            {

                $obj -> descripcion = $descripcion;
                $obj -> espos = $esPos;
                $obj -> codigoadm=$codigoAdm;

                R::store($obj);
                $res->status(200);
                $arrRtn['mensaje'] = 'Operacion exitosa!!';
                echo json_encode($arrRtn);
            }else{
                throw new Exception('No Existe Registro');
            }
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function delete($app,$id){
        try {
            $res=$app->response();
            $obj = R::findOne(self::$tabla,'id=?',array($id));
            if ($obj)
            {
                R::trash($obj);
                $res->status(200);
                $arrRtn['mensaje'] = 'Operacion exitosa!!';
                echo json_encode($arrRtn);
            }else{
                throw new Exception('No Existe Registro');
            }


        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

}