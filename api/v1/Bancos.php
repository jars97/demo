<?php

/**
 * Created by PhpStorm.
 * User: joserodriguez
 * Date: 17/02/17
 * Time: 08:09 PM
 */
class Bancos
{
    private $instancia;
    private $clase='maestrobancos';

    public function __construct()
    {
        $this -> instancia=Conexion::getInstance();
    }


    public function listAll($app){
        $sql = "select *  from ".$this->clase;
        try {
            $Db = $this -> instancia -> getConnection();

            $stmt = $Db->prepare($sql);
            $stmt->execute();
            $obj = $stmt->fetchAll(PDO::FETCH_OBJ);

            $db = null;

            return $obj;
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function create($app){
        $sql = "insert into maestrobancos (descripcion,codigoAdm,esPos) values (:descripcion,:codigoAdm,:esPos)";
        try {
            $res=$app->response();
            $Db = $this -> instancia -> getConnection();

            $request = $app->request();
            $body = $request->getBody();
            $input = json_decode($body);
            $descripcion=(string) $input->descripcion;
            $esPos=(string) $input->esPos;
            $codigoAdm=(string) $input->codigoAdm;

            $stmt = $Db->prepare($sql);
            $stmt->bindParam("descripcion", $descripcion);
            $stmt->bindParam("esPos",$esPos);
            $stmt->bindParam("codigoAdm", $codigoAdm);
            $stmt->execute();

            $res->status(200);
            $arrRtn['mensaje'] = 'Operacion exitosa!!';
            echo json_encode($arrRtn);
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function readById($app,$id){
        $sql = "select *  from maestrobancos where id=:id";
        try {
            $Db = $this -> instancia -> getConnection();

            $stmt = $Db->prepare($sql);
            $stmt->bindParam("id", $id);
            $stmt->execute();

            $obj = $stmt->fetchObject();

            if ($obj)
            {
                return $obj;
            }else{
                throw new Exception('No Existe el registro');
            }
            $db = null;

            return $obj;
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function update($app,$id){
        $sql = "update maestrobancos set codigoAdm=:codigoAdm, descripcion=:descripcion, esPos=:esPos where id=:id";
        try {
            $res=$app->response();
            $Db = $this -> instancia -> getConnection();

            $request = $app->request();
            $body = $request->getBody();
            $input = json_decode($body);
            $descripcion=(string) $input->descripcion;
            $esPos=(string) $input->esPos;
            $codigoAdm=(string) $input->codigoAdm;

            $stmt = $Db->prepare($sql);
            $stmt->bindParam("id", $id);
            $stmt->bindParam("descripcion", $descripcion);
            $stmt->bindParam("esPos",$esPos);
            $stmt->bindParam("codigoAdm", $codigoAdm);
            $stmt->execute();

            $res->status(200);
            $arrRtn['mensaje'] = 'Operacion exitosa!!';
            echo json_encode($arrRtn);
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

    public function delete($app,$id){
        $sql = "delete from maestrobancos where id=:id";
        try {
            $res=$app->response();
            $Db = $this -> instancia -> getConnection();
            $stmt = $Db->prepare($sql);
            $stmt->bindParam("id", $id);
            $stmt->execute();

            $res->status(200);
            $arrRtn['mensaje'] = 'Operacion exitosa!!';
            echo json_encode($arrRtn);
        } catch(PDOException $e) {
            $app->response()->status(400);
            $app->response()->header('X-Status-Reason', $e->getMessage());
            $arrRtn['error'] = $e->getMessage();
            echo json_encode($arrRtn);
        }

    }

}