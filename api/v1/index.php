<?php

require '../Slim/Slim.php';
require '../Slim/Middleware.php';


require '../Conexion.php';
require 'librerias.php';
require 'Bancos.php';


\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$librerias = new librerias();
$librerias->cors();






/////////////////////////////////////////////////////////////////////////////////////
$app->get('/', function() use($app){


    echo "Bienvenido al API REST de mi aplicacion ";


});
/////////////////////////////////////////////
// Bancos
$app->get('/bancos', function() use ($app){
    $app->response()->header('Content-Type', 'application/json');
    $bancos=new Bancos();
    $obj=$bancos -> listAll($app);
    if ($obj){echo json_encode($obj,JSON_NUMERIC_CHECK);}

});
$app->get('/bancos/:id', function($id) use ($app){
    $app->response()->header('Content-Type', 'application/json');
    $bancos=new Bancos();
    $obj=$bancos -> readById($app,$id);
    if ($obj){echo json_encode($obj,JSON_NUMERIC_CHECK);}

});
$app->post('/bancos', function() use ($app){
    $app->response()->header('Content-Type', 'application/json');
    $bancos=new Bancos();
    $bancos->create($app);
});
$app->put('/bancos/:id', function($id) use ($app){
    $app->response()->header('Content-Type', 'application/json');
    $bancos=new Bancos();
    $bancos->update($app,$id);
});
$app->delete('/bancos/:id', function($id) use ($app){
    $app->response()->header('Content-Type', 'application/json');
    $bancos=new Bancos();
    $bancos->delete($app,$id);
});
//////////////////////////////////////////////
/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();




