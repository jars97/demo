<?php

/**
 * Created by PhpStorm.
 * User: joserodriguez
 * Date: 23/03/16
 * Time: 05:33 PM
 */
class Conexion
{
    private $dbhost="localhost";
    private $dbuser="demo";
    private $dbpass="demo";
    //private $dbuser;
    //private $dbpass;
   private $dbname="demo";
   // private $dbname="hotel";
    private static $_instance;
    private $_connection;

    public static function getInstance()
    {
        if(!self::$_instance) // If no instance then make one
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    private function __construct(){
        try {
            //$this -> dbuser= $_SESSION["usuario"];
            //$this -> dbpass=$_SESSION["pass"];
            $datosconex="mysql:host=".$this->dbhost.";dbname=".$this->dbname;
            $this->_connection=new PDO($datosconex, $this->dbuser, $this->dbpass);
            $this -> _connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        }catch (PDOException $e) {
            echo '{"estatus":{"text":'. $e->getMessage() .'}}';
        }
    }

    private function __clone() { }

    public function getConnection()
    {
        return $this->_connection;
    }

}