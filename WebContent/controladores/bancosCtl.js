/**
 * Created by joserodriguez on 07/05/16.
 */
//------------------------------------------------------------------------------------------------------
angular
    .module('mainApp')
    .controller('bancosCtl',bancosCtl);

bancosCtl.$inject = ['$scope','modeloBancos','$routeParams','$location','$route','mensaje'];

function bancosCtl($scope,modeloBancos,$routeParams,$location,$route,mensaje) {

    $scope.formData = {};

    //----- definiciones
    var vm = this;
    vm.load = load;
    vm.editar = editar;
    vm.create = create;
    vm.eliminar = eliminar;
    vm.shouldBeDisabled = shouldBeDisabled;
    //----------------------------------------

    //----- funciones

    function load() {
        modeloBancos.getAll()
            .success(function (res, status, headers, config) {
               // console.log(res);
                vm.todos = res;
            })
            .error(function (data, status, header, config) {
                mensaje.dameVentanaError('md', data.mensaje);
            });
    };


    function eliminar(id) {
        if (confirm("Estas seguro de realizar esta operacion?")) {
            modeloBancos.delete(id)
                .success(function (data, status, headers, config) {
                    mensaje.dameVentana('sm', data.mensaje);
                    $route.reload();
                })
                .error(function (data, status, header, config) {
                    mensaje.dameVentanaError('md', data.mensaje);

                });
        }

    }

    function editar(id) {
        $location.path('/bancos-edit/' + id);
    }

    function create() {
        $location.path('/bancos-create');
    }

    function shouldBeDisabled(item) {
        if (item.esPos) {
            return true;
        } else {
            return false;
        }
    };
};
