/**
 * Created by joserodriguez on 18/02/17.
 */
angular
    .module('mainApp')
    .controller('bancosCreateCtl',bancosCreateCtl);

bancosCreateCtl.$inject = ['$scope','modeloBancos','$routeParams','$location','$route','mensaje'];

function bancosCreateCtl($scope,modeloBancos,$routeParams,$location,$route,mensaje) {



    //----- definiciones
    var vm = this;
    vm.create=create;
    vm.volver=volver;

    //----------------------------------------

    //----- funciones



    function volver(){
        $location.path('/bancos-lista');
    };

    function create(){
        //var data =$('#formulario').serialize();
        if (vm.bancos.esPos==undefined){
            vm.bancos.esPos=0;
        }
        var datos ={"descripcion":vm.bancos.descripcion,"codigoAdm":vm.bancos.codigoAdm,"esPos":vm.bancos.esPos};
        var data1=angular.toJson(datos);
        var dd= angular.toJson(vm.bancos);
        console.log(dd);
        modeloBancos.create(data1)
            .success(function (res, status, headers, config) {
                mensaje.dameVentana('sm',res.mensaje);
                $location.path('/bancos-lista');
            })
            .error(function (data, status, header, config) {
                mensaje.dameVentanaError('md',data.mensaje);
            });
    };
};
