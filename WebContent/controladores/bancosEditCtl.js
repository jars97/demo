angular
    .module('mainApp')
    .controller('bancosEditCtl',bancosEditCtl);

bancosEditCtl.$inject = ['$scope','modeloBancos','$routeParams','$location','$route','mensaje'];

function bancosEditCtl($scope,modeloBancos,$routeParams,$location,$route,mensaje) {



    //----- definiciones
    var vm = this;
    vm.load = load;
    vm.volver=volver;
    vm.update=update;
    //----------------------------------------

    //----- funciones

    function load() {
        modeloBancos.get($routeParams.id)
            .success(function (res, status, headers, config) {
                //console.log(res);
                vm.bancos = res;
                if (vm.bancos.esPos){
                    vm.activo=true;
                }else{
                    vm.activo=false;
                }
            })
            .error(function (data, status, header, config) {
                mensaje.dameVentanaError('md', data.mensaje);
            });
    };

    function volver(){
        $location.path('/bancos-lista');
    };

    function update(){
        //var data =$('#formulario').serialize();
        console.log(vm.activo);
        var datos ={"descripcion":vm.bancos.descripcion,"codigoAdm":vm.bancos.codigoAdm,"esPos":vm.activo};
        var data1=angular.toJson(datos);

       // console.log(data1);
        modeloBancos.update($routeParams.id,data1)
            .success(function (res, status, headers, config) {
                mensaje.dameVentana('sm',res.mensaje);
                $location.path('/bancos-lista');

            })
            .error(function (data, status, header, config) {
                mensaje.dameVentanaError('md', data.mensaje);

            });
    };
};
