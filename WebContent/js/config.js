mainApp.config(function($routeProvider){


	$routeProvider

		.when('/',
			{
				templateUrl:'WebContent/vistas/bancos-lista.html',
				controller:'bancosCtl',
				controllerAs :"vm"
			})
		.when('/bancos-lista',
			{
				controller:'bancosCtl as vm',
				templateUrl:'WebContent/vistas/bancos-lista.html'

			})
		.when('/bancos-edit/:id',
			{
				controller:'bancosEditCtl',
				templateUrl:'WebContent/vistas/bancos-edit.html',
				controllerAs :"vm"

			})
		.when('/bancos-create',
			{
				controller:'bancosCreateCtl',
				templateUrl:'WebContent/vistas/bancos-create.html',
				controllerAs :"vm"

			})
		.otherwise({ redirectTo:'/'});




});