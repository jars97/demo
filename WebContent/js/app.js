/**
 * Created by joserodriguez on 07/05/16.
 */
var mainApp = angular.module('mainApp', ['ngRoute','ui.bootstrap','ngTouch','ngResource','ngAnimate']);

mainApp.service('API', function() {
    this.dameContexto = function(){
        return "http://localhost/demo/api/v1"

    }
});

mainApp.service('mensaje', function($modal) {
    this.dameVentana = function(size,mensaje){
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'mensajes/mensaje.html',
            controller: 'mensajeCtl',
            size: size,
            resolve: {
                mensaje: function () {
                    return mensaje;
                }
            }
        });
    }
    this.dameVentanaError = function(size,mensaje){
        var modalInstance = $modal.open({
            animation: true,
            templateUrl: 'mensajes/mensaje-error.html',
            controller: 'mensajeCtl',
            size: size,
            resolve: {
                mensaje: function () {
                    return mensaje;
                }
            }
        });
    }

});









